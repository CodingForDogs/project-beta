import React, { useState } from "react";

function AddSalesPersonForm() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employee_id, setEmployeeId] = useState('');

  const employeeIDChange = (event) => {
    setEmployeeId(event.target.value);
  }

  const firstNameChange = (event) => {
    setFirstName(event.target.value);
  }

  const lastNameChange = (event) => {
    setLastName(event.target.value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};

    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employee_id;

    const salespersonUrl = 'http://localhost:8090/api/salespeople/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const salesperson = await fetch(salespersonUrl, fetchConfig);
    if (salesperson.ok) {

      setEmployeeId('');
      setFirstName('');
      setLastName('');

    }
  }
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Sales Person</h1>
          <form id="create-conference-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                placeholder="First Name"
                required type="text"
                name="first_name"
                id="first_name"
                className="form-control"
                onChange={firstNameChange}
                value={firstName}
              />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Last Name"
                required type="text"
                name="last_name"
                id="last_name"
                className="form-control"
                onChange={lastNameChange}
                value={lastName}
              />
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Employee ID"
                required type="text"
                name="employee_id"
                id="employee_id"
                className="form-control"
                onChange={employeeIDChange}
                value={employee_id}
              />
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default AddSalesPersonForm;
