import React, { useEffect, useState } from "react";


function SalesPersonList() {
    const [salesPersons, setSalesPersons] = useState([]);

    async function loadSalesPersons() {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalesPersons(data.salespeople);
        }
    }

    useEffect(() => {
        loadSalesPersons();
    }, []);

    return (
    <>
    <h1>Salespeople</h1>
       <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {salesPersons?.map(salespeople => {
                    return (
                        <tr key={salespeople.id}>
                            <td>{salespeople.employee_id}</td>
                            <td>{salespeople.first_name}</td>
                            <td>{salespeople.last_name}</td>
                        </tr>
                    );
                })}
            </tbody>
       </table>
    </>
    );
}

export default SalesPersonList;
