import React, { useState, useEffect } from "react";

function SalesHistory() {

    const [salespeople, setSalesPeople] = useState([]);
    const [salesperson, setSalesPerson] = useState('');
    const [sales, setSales] = useState([]);


    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesPerson(value);
    }


    async function fetchSalesPeople () {
        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salespersonUrl);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespeople);
        }
    }

    async function fetchSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }

    useEffect(() => {
        fetchSalesPeople();
        fetchSales();
    }, []);

    return (
       <>
       <h1>Salesperson History</h1>
       <div className="form-floating mb-3">
            <select required
                name="sales_person"
                id="sales_person"
                className="form-select"
                onChange={handleSalesPersonChange}
                value={salesperson}
            >
                    <option value="">Select a Salesperson</option>
                        {salespeople?.map(sales_person => {
                            return (
                                <option key={sales_person.id} value={sales_person.employee_id}>
                                    {sales_person.first_name} {sales_person.last_name}
                                </option>

                            );
                        })}
            </select>
        </div>
        <table className='table table-striped'>
            <thead>
                <tr>
                    <th>Sales Person</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.filter(sale => sale.salesperson.employee_id == salesperson).map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>${sale.price}.00</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
       </>
    );
};

export default SalesHistory;
