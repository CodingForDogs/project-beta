import { useEffect, useState } from "react"


function AppointmentsList() {

    const [appointments, setAppointments] = useState([])
    const [inventoryVins, setinventoryVins] = useState([])


    async function getData() {
        const response = await fetch('http://localhost:8080/api/appointments/')
        if (response.ok) {
            const data = await response.json()
            setAppointments(data.appointments)
        }
    }


    const Cancel = ((id)=> {
        if(window.confirm("Do you want to cancel?")) {
            fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {method:"PUT"},).then(()=> {}).catch((err)=>{
                console.log(err.message)
            })
        }
    })


    const Finish = ((id)=> {
        if(window.confirm("Do you want to finish?")) {
            fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {method:"PUT"},).then(()=> {}).catch((err)=>{
                console.log(err.message)
            })
        }
    })

    useEffect(() => {
        getData()
        getAutomobiles()
}, [])


    async function getAutomobiles() {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
            let inventoryVinsTemp = []

            const data = await response.json()

            for (let auto of data.autos) {
                inventoryVinsTemp.push(auto.vin)
            }
            setinventoryVins(inventoryVinsTemp)
        }
    }


    function isVip(appointmentVin) {
        const sold = inventoryVins.find(invenVin => invenVin == appointmentVin)

        if (appointmentVin === sold) {
            return "Yes"
        } else {
            return "No"
        }
    }


    return (
        <div>
            <h1>Service Appointments</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                </tr>
            </thead>
            <tbody>
                {appointments.filter(appointments => appointments.status.toLowerCase().includes("created")).map(appointment => {
                    return (
                        <tr key={appointment.id}>
                            <td>{ appointment.vin }</td>
                            <td>{ isVip(appointment.vin) }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ new Date(appointment.date_time).toLocaleDateString() }</td>
                            <td>{ new Date(appointment.date_time).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' }) }</td>
                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td>{ appointment.reason }</td>
                            <td>
                                <button onClick={() => {Cancel(appointment.id)}}>Cancel</button>
                            </td>
                            <td>
                                <button onClick={() => {Finish(appointment.id)}}>Finish</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
        </div>
    );
}

export default AppointmentsList;
