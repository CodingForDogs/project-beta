import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechniciansList from './TechniciansList';
import TechnicianForm from './TechnicianForm';
import AppointmentsList from './AppointmentsList';
import AppointmentForm from './AppointmentForm';
import AppointmentsHistory from './AppointmentsHistory';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './ModelsList';
import SalesPersonList from './sales_frontend/ListSalesPerson';
import AddSalesPersonForm from './sales_frontend/AddSalesPersonForm';
import ListCustomers from './sales_frontend/ListCustomer';
import AddCustomerForm from './sales_frontend/AddCustomerForm';
import SalesList from './sales_frontend/ListSales';
import SalesForm from './sales_frontend/AddSalesForm';
import SalesHistory from './sales_frontend/HistorySales';
import ModelForm from './ModelForm';
import AutomobilesList from './AutomobilesList';
import AutomobileForm from './AutomobileForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="" element={<ManufacturersList />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route path="" element={<ModelsList />} />
            <Route path="create" element={<ModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<AutomobilesList />} />
            <Route path="create" element={<AutomobileForm />} />
          </Route>
          <Route path="technicians">
            <Route path="" element={<TechniciansList />} />
            <Route path="create" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route path="" element={<AppointmentsList />} />
            <Route path="create" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentsHistory />} />
          </Route>
          <Route path="salesperson" >
            <Route path="" element={<SalesPersonList />} />
            <Route path="create" element={<AddSalesPersonForm />} />
          </Route>
          <Route path="customer">
            <Route path="" element={<ListCustomers />} />
            <Route path="create" element={<AddCustomerForm />} />
          </Route>
          <Route path="sales" >
            <Route path="" element={<SalesList />} />
            <Route path="create" element={<SalesForm />} />
            <Route path="history" element={<SalesHistory />}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
