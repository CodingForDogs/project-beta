import { useEffect, useState } from "react";

function ModelForm() {
    const [manufacturers, setManufacturers] = useState([]);
    const [name, setModelName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturer;

        const postUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const postRes = await fetch(postUrl, fetchConfig);
        if (postRes.ok) {
            setManufacturer('');
            setModelName('');
            setPictureUrl('');
        } else {
            console.error("Problem posting new model to inventory-api line 25");
        }
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const fetchData = async () => {
        const makesUrl = 'http://localhost:8100/api/manufacturers/';
        const makesRes = await fetch(makesUrl);
        if (makesRes.ok) {
            const makesData = await makesRes.json();
            setManufacturers(makesData.manufacturers);
        } else {
            console.error("Error getting data from inventory-api");
        }

    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a Model</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <select
                              onChange={handleManufacturerChange}
                              name="manufactuer"
                              required id="manufacturer"
                              value={manufacturer}
                              className="form-select"
                            >
                                <option value="">Choose a Make</option>
                                {manufacturers && manufacturers.map(make => {
                                    return (
                                        <option key={make.id} value={make.id}>
                                            {make.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                              name="name"
                              placeholder="name"
                              onChange={handleNameChange}
                              required type="text"
                              id="name"
                              value={name}
                              className="form-control"
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                              name="pictureUrl"
                              placeholder="Picture Url"
                              onChange={handlePictureUrlChange}
                              required type="url"
                              id="pictureUrl"
                              value={pictureUrl}
                              className="form-control"
                            />
                            <label htmlFor="pictureUrl">Picture Url</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
    )
}
export default ModelForm;
