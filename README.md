# CarCar

Team:

- Valeria - Service microservice
- Faith - Sales microservice

## Design

## Service microservice

I created three models: 1. Technician Model, 2. AutomobileVO model, and 3. Appointment model. I created view functions which I implemented in the Services components in the front-end.

## Sales microservice

Starting off the Sales microservices, I created the models, view functions and encoders. I then set up my insomnia links, made a couple of objects through insomnia, and migrated.
